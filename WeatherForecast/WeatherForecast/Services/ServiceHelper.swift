//
//  ServiceHelper.swift
//  WeatherForecast
//
//  Created by Anand Yadav on 10/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit
import Alamofire

enum ServiceHelper: URLRequestConvertible {
    case getWeather(parameters: Parameters)
    case getWeatherForecast(parameters: Parameters)
    
    var method: HTTPMethod {
        switch self {
        case .getWeather:
            return .get
        case .getWeatherForecast:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getWeather(let username):
            return "/weather"
        case .getWeatherForecast(let username):
            return "/forecast"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .getWeather(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .getWeatherForecast(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        debugPrint(urlRequest)
        return urlRequest
    }
}
