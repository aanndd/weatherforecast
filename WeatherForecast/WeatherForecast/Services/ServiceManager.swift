//
//  ServiceManager.swift
//  WeatherForecast
//
//  Created by Anand Yadav on 10/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class ServiceManager: NSObject {
    static func getWeatherData(param: [String:Any],
                                    success: @escaping (_ response: Any?) -> Void,
                                    failure: @escaping (_ error: Error?) -> Void) {
        
        BaseService().apiRequest(url: ServiceHelper.getWeather(parameters: param), success: { (response) in
            let weatherData = WeatherData.map(JSONData:response as! Data)!
            success(weatherData)
        }) { (error) in
            failure(error)
        }
    }
    
    static func getWeatherForecastData(param: [String:Any],
                                    success: @escaping (_ response: Any?) -> Void,
                                    failure: @escaping (_ error: Error?) -> Void) {
        
        BaseService().apiRequest(url: ServiceHelper.getWeatherForecast(parameters: param), success: { (response) in
            let weatherData = ForecastData.map(JSONData:response as! Data)!
            success(weatherData)
        }) { (error) in
            failure(error)
        }
    }
}
