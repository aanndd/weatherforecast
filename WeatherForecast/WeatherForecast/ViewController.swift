//
//  ViewController.swift
//  WeatherForecast
//
//  Created by Anand Yadav on 10/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var topConstraintSearchView: NSLayoutConstraint!
    var cityData = [WeatherData]()
    var cityArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        setUpView()
        // Do any additional setup after loading the view.
        cityArray.removeAll()
        if let cities = UserDefaultManager.value {
            cityArray = cities
        }
        for city in cityArray {
            AYProgressHUD.sharedInstance.showBlurView(withTitle: "Loading...")
            ServiceManager.getWeatherData(param: ["q" : city, "appid" : API_KEY], success: { (response) in
                AYProgressHUD.sharedInstance.hide()
                self.cityData.append(response as! WeatherData)
                self.tableView.reloadData()

            }) { (error) in
                AYProgressHUD.sharedInstance.hide()
                debugPrint(error as Any)
            }
        }
    }
    
    func setUpView(){
        searchView.layer.shadowColor = UIColor.gray.cgColor
        searchView.layer.shadowOpacity = 1
        searchView.layer.shadowOffset = .zero
        searchView.layer.shadowRadius = 5
    }

    @IBAction func btnAddClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.txtCity.becomeFirstResponder()
            self.topConstraintSearchView.constant = 150
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnAddCityClicked(_ sender: Any) {
        txtCity.resignFirstResponder()
        for city in cityArray {
            if(txtCity.text!.caseInsensitiveCompare(city) == ComparisonResult.orderedSame) {
                self.toastMessage("City already added!")
                return
            }
        }
        topConstraintSearchView.constant = -250
        AYProgressHUD.sharedInstance.showBlurView(withTitle: "Loading...")
        ServiceManager.getWeatherData(param: ["q" : txtCity.text!, "appid" : API_KEY], success: { (response) in
            AYProgressHUD.sharedInstance.hide()
            debugPrint(response!)
            self.cityData.append(response as! WeatherData)
            self.cityArray.append(self.txtCity.text!)
            self.txtCity.text = ""
            UserDefaultManager.value = self.cityArray
            self.tableView.reloadData()
        }) { (error) in
            AYProgressHUD.sharedInstance.hide()
            self.toastMessage(error!.localizedDescription)
            debugPrint(error as Any)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cityData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell") as! WeatherCell
        print(cityData[indexPath.row])
        cell.lblCity.text = cityData[indexPath.row].name!
        cell.lblTemp.text = String(format: "%.0f", cityData[indexPath.row].main!.temp! - 273.15) + "°C"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showWeatherVC", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showWeatherVC" {
            if let dest = segue.destination as? WeatherViewController {
                dest.weather = cityData[tableView.indexPathForSelectedRow!.row]
            }
        }
    }
}

