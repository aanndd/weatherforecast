//
//  WeatherViewController.swift
//  WeatherForecast
//
//  Created by Anand Yadav on 16/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    var weather:WeatherData?
    var forecastList = [List]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getWeatherForecastData(city: (weather?.name)!)
        setupUI()
    }
    
    func setupUI() {
        if let cod = weather?.cod {
            weatherIcon.image = UIImage(named:(weather?.getStatus(condition: cod))!)
        }
        cityLabel.text = weather?.name
        tempLabel.text = String(format: "%.0f", (weather?.main?.temp)! - 273.15) + "°C"
        
        if let temp = weather?.main?.pressure {
            pressureLabel.text = String(temp)
        }
        if let humidity = weather?.main?.humidity {
            humidityLabel.text = String(humidity)
        }
    }
    
    func getWeatherForecastData(city:String) {
        AYProgressHUD.sharedInstance.showBlurView(withTitle: "Loading...")
        ServiceManager.getWeatherForecastData(param: ["q" : city, "appid" : API_KEY], success: { (response) in
            AYProgressHUD.sharedInstance.hide()
            if let forecastData = response as? ForecastData {
                self.forecastList = forecastData.list!
                self.collectionView.reloadData()
            }
        }) { (error) in
            AYProgressHUD.sharedInstance.hide()
            debugPrint(error as Any)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecastList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as! ForecastCollectionViewCell
        if let timestamp = forecastList[indexPath.row].dt {
            cell.lblHour.text = Double(timestamp).dateFormatted(withFormat: "HH:mm")
        }
        cell.lblTemp.text = String(format: "%.0f", forecastList[indexPath.row].main!.temp! - 273.15) + "°C"
        if let cod = weather?.cod {
            cell.weatherIcon.image = UIImage(named:(weather?.getStatus(condition: cod))!)
        }
        return cell
    }

}
