//
//  Helper.swift
//  WeatherForecast
//
//  Created by Anand Yadav on 12/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

struct UserDefaultManager {
    static let key = "weather.city"

    static var value: [String]? {
        get {
            return UserDefaults.standard.object(forKey: key) as? [String]
        }
        set {
            if newValue != nil {
                UserDefaults.standard.set(newValue, forKey: key)
            } else {
                UserDefaults.standard.removeObject(forKey: key)
            }
        }
    }
}
