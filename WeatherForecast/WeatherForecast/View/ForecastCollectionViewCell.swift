//
//  ForecastCollectionViewCell.swift
//  WeatherForecast
//
//  Created by Anand Yadav on 17/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class ForecastCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblHour: UILabel!
    @IBOutlet weak var lblTemp: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
}
